﻿Shader "LordShader/01_ScrollUVs" 
{
	Properties {
		_MainTint ("Diffuse Color", Color) = (1,1,1,1)			//颜色属性，可以在u3d inspector面板控制该变量
		_MainTex ("Base (RGB)", 2D) = "white" {}				//纹理贴图
		_ScrollSpeedX(" X Scroll Speed",Range(0,10)) = 2		//X方向上的滚动速度
		_ScrollSpeedY(" Y Scroll Speed",Range(0,10)) = 2		//Y方向上的滚动速度
	}
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert

		fixed4 _MainTint;
		sampler2D _MainTex;
		fixed _ScrollSpeedX;
		fixed _ScrollSpeedY;

		struct Input {
			float2 uv_MainTex;
		};

		//可以参考 ：http://blog.csdn.net/wolf96/article/details/41801117

		void surf (Input IN, inout SurfaceOutput o) {
			//保存贴图的uv坐标
			fixed2 scrollUV = IN.uv_MainTex;
			//计算uv随时间的增量
			fixed addU  = _ScrollSpeedX * _Time;
			fixed addV  = _ScrollSpeedY * _Time;

			fixed oldv  = scrollUV.y;
			fixed oldu	= scrollUV.x;
			//uv累加
			scrollUV += fixed2(addU,addV);

			//scrollUV.y = oldv;
			//scrollUV.x = oldu;  //只改变v
			//根据新计算出UV值进行采样
			half4 c = tex2D(_MainTex,scrollUV);
			o.Albedo = c.rgb * _MainTint;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
