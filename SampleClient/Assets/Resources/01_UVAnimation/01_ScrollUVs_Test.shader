﻿Shader "LordShader/01_ScrollUVs_Test" {
	Properties {
		_MainTint ("Diffuse Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_ScrollSpeedX(" X Scroll Speed",Range(0,10)) = 2
		_ScrollSpeedY(" Y Scroll Speed",Range(0,10)) = 2
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		fixed4 _MainTint;
		sampler2D _MainTex;
		fixed _ScrollSpeedX;
		fixed _ScrollSpeedY;
		half _Glossiness;
		half _Metallic;

		struct Input {
			float2 uv_MainTex;
		};

		void surf (Input IN, inout SurfaceOutputStandard o) {
			//保存贴图的uv坐标
			fixed2 scrollUV = IN.uv_MainTex;
			//计算uv随时间的增量
			fixed addU  = _ScrollSpeedX * _Time;
			fixed addV  = _ScrollSpeedY * _Time;

			//uv累加
			scrollUV += fixed2(addU,addV);

			//根据新计算出UV值进行采样
			half4 c = tex2D(_MainTex,scrollUV);
			o.Albedo = c.rgb * _MainTint;
			
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
