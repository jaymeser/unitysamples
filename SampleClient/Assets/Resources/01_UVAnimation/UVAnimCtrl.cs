﻿using UnityEngine;
using System.Collections;

public class UVAnimCtrl : MonoBehaviour {

    /**
     * 通过脚本修改纹理的UV坐标
     * */

	public float m_speedX = 0.0250f;
	public float m_speedY = 0.0250f;
    // Use this for initialization
    public static string m_riverMatName = "";
    Renderer[] m_renders;
    
	void Start () {
        m_renders = GetComponentsInChildren<Renderer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (m_renders == null)
            return;
        for (int i = 0; i < m_renders.Length; ++i)
        {
            if (m_renders[i] && m_renders[i].material.name.Equals("RiverWater2 (Instance)"))
            {
                float offsetU = Time.time * m_speedX;
                float offsetV = Time.time * m_speedY;
                m_renders[i].material.mainTextureOffset = new Vector2(offsetU, offsetV);
            }
        }
    }
}
