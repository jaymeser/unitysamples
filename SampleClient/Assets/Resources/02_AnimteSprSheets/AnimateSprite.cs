﻿using UnityEngine;
using System.Collections;

public class AnimateSprite : MonoBehaviour {

	float timeVal = 0.0f;
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate()
	{
		timeVal = Mathf.Ceil(Time.time % 16);
		//传递timeVal值给Shader中的_TimeValue对象
		transform.GetComponent<MeshRenderer>().material.SetFloat("_TimeValue",timeVal);
		//Debug.Log("timeVal " + timeVal);
	}
}
